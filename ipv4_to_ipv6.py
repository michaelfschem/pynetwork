from scapy.all import *
from scapy.layers.inet6 import IPv6
from scapy.layers.inet import IP

# rdpcap comes from scapy and loads in our pcap file
packets = rdpcap('pcap/file_alert.pcap')
# packets = rdpcap('pcap/v6-http.cap')

for key, packet in enumerate(packets):
    print()
    if packet.haslayer(IP):

        new_packet = packet

        if packet.payload.src == '192.168.1.158':
            ipv6 = IPv6(src='fe80::0123:4567', dst='fe80::0123:4568')
        else:
            ipv6 = IPv6(src='fe80::0123:4568', dst='fe80::0123:4567')

        ipv6.payload = packet.payload.payload

        new_packet.payload = ipv6

        packet = new_packet

        packet.type = 34525

        print()

        # print(new_packet.summary())
wrpcap('/tmp/out.pcap', packets)

